#!/bin/bash

set -o errexit

# The password store initialization should be automatic, and it should only be
# done once.
if ! [ -d "$HOME/.password-store" ]
then
  # pass needs a GPG key, so we generate a standard one: a non-expiring RSA key
  # of length 2048 bits, with no passphrase (for convenience), enabled for all
  # usages (cert, sign, encrypt, auth).
  cat > /tmp/gen-key-script <<-EOF
		%no-protection
		Key-Type: 1
		Key-Length: 2048
		Name-Real: Foo Bar
		Name-Email: foo@bar
		Expire-Date: 0
	EOF

  gpg --batch --gen-key /tmp/gen-key-script
  rm /tmp/gen-key-script

  # init the password store with that key
  pass init foo@bar
fi

# Execute the given command. By default, this will start the bridge CLI frontend.
exec "$@"
