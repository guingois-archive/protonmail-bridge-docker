# ProtonMail Bridge Docker image

A lightweight Docker image for the ProtonMail bridge (excluding the GUI
front-end), weighting approximately 50MB.

**WARNING**: This is a personal experiment, published without any
warranty of any kind. If you plan on using it, I invite you to have a
careful look at the code and consider whether it might compromise the
security of your ProtonMail accounts. If in doubt, _do not use it_. This
is especially true if you are not the only user with access to the
Docker daemon on your system.

## Building the image

```sh
cd docker

docker build \
  --tag protonmail-bridge:latest \
  --tag protonmail-bridge:v1.2.7-beta.3 \
  .
```

## Using the image

Again, a warning: the ProtonMail credentials created by the Bridge will
basically be stored unprotected in the password store. Anyone with
access to the password store folder will therefore have access to those
credentials.

### With a one-shot interactive container:

Start the bridge with the interactive (CLI) front-end, while binding its
IMAP and SMTP ports to your host local interface. The ProtonMail
credentials will not persist after the container stops.

```sh
docker run \
  --rm \
  --interactive \
  --tty \
  --publish 127.0.0.1:1025:1025 \
  --publish 127.0.0.1:1143:1143 \
  protonmail-bridge

# >>> login
# >>> info
# >>> exit
```

### As a detached container:

The goal is to have the bridge running as a detached container. We need
to first configure it with the interactive (CLI) front-end and then
restart it with the non-interactive one, while keeping the config
established in the previous run.

This requires the ProtonMail credentials to be stored in a Docker
volume, and since Docker won't automatically remove volumes, **the
credentials will persist until you explicitly remove the volume**.

```sh
docker volume create protonmail-bridge

docker run \
  --rm \
  --volume protonmail-bridge:/home/foo \
  --interactive \
  --tty \
  protonmail-bridge \
  proton-bridge --cli

# >>> login
# >>> info
# (use the given info to connect your mail client)
# >>> exit

docker run \
  --rm \
  --volume protonmail-bridge:/home/foo \
  --detach \
  --publish 127.0.0.1:1025:1025 \
  --publish 127.0.0.1:1143:1143 \
  --name proton-bridge-server \
  protonmail-bridge \
  proton-bridge --noninteractive

# Once you're done:
docker stop proton-bridge-server
docker volume rm protonmail-bridge
```

## License

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or any later
version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
